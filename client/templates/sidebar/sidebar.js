Template.sidebar.helpers({
    showChats: ()=> {
    	return(Session.get('showChats'))
    },
    showNotification: ()=>{
      return(Session.get('showNotification'))
    }
});
