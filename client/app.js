Template.mainLayout.onCreated(function(){
	var self = this;	
	self.autorun(function() {
		if(!!Meteor.user()){
			self.subscribe('currentUserDetails');
		}
	});
});
Template.mainLayout.helpers({
	currentUsername: function(){
		return UserCustom.findOne({userId: Meteor.userId()}).name;
	}
});