Posts = new Mongo.Collection("posts");
Meteor.methods({
  newPost: function (text) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
    let newPost={
          text: text,
          createdAt: new Date(),
          owner: Meteor.userId(),
          username: Meteor.user().username,
          like:[],
          likes:0,
          comments:[]
        };
    Posts.insert(newPost,function(error,result){
      if (error) {
          console.log("There was some error")
        }
      else {
        UserCustom.update({userId:Meteor.userId()},{$addToSet: {posts:result}});
      }
    });
  },
  deletePost: function (postId) {
    // if(this.owner==Meteor.userId()){
      Posts.remove({_id: postId});
    // }
  },
  addLike: function (post,userId) {
    if (Meteor.user()){
          if(post.like.indexOf(userId)=== -1){
            Posts.update({_id:post._id},{$inc: { likes: 1 }},function(error,rows){
              if (error) {
                console.log("There was some error");
              }
              else {
                Posts.update({_id:post._id},{$addToSet: {like:userId}},function(error,rows){
                  if (error) {
                    console.log("There was some error");
                  }
                  else {
                    UserCustom.update({userId:Meteor.userId()},{$addToSet: {likes:post._id}});
                  }
                });
              }
            });
          }
          else{
          	Posts.update({_id:post._id},{$inc: { likes: -1 }},function(error,rows){
              if (error) {
                console.log("There was some error");
              }
              else {
                Posts.update({_id:post._id},{$pull: {like:userId}},function(error,rows){
                  if (error) {
                    console.log("There was some error");
                  }
                  else
                  {
                    UserCustom.update({userId:Meteor.userId()},{$pull: {likes:post._id}});
                  }
                });
              }
            });
          }
      }
  },
  addComment: function(postId,userId,text){
    if(!Meteor.user()){
      throw new Meteor.error(401, "Please log in");
    }
    else{
          let commentator= UserCustom.findOne({userId:userId},{fields:{name:1, url:1}});
          let newComment={
            postId:postId,
            name:commentator.name,
            url:commentator.url,
            userId:userId,
            text:text,
            createdAt: new Date()
          };
          UserComments.insert(newComment,function(error,result){
            if (error) {
              console.log("There was some error in inserting comment in comment schema");
            }
            else {
                UserCustom.update({userId:Meteor.userId()},{$addToSet: {comments:result}});
                Posts.update({_id:postId},{$push: {comments:result }},function(error,rows){
                  if(error){
                    console.log("there was some error in commenting");
                  }
                  else{
                    let whosePost=Posts.findOne({_id:postId},{fields:{owner:1}});//to whom send notification
                    Notifications.insert({
                      to:whosePost.owner,
                      type:"comment",
                      postId:postId,
                      by:Meteor.userId(),
                      byName:commentator.name,
                      byurl:commentator.url
                    });
                  }
                });
              }
          });
    };
},
  addUser:function(name){
    User.insert({
      name:name,
      password:"letmein"
    });
  }
});
