UserCustom=new Mongo.Collection("userCustom");
Meteor.methods({
	addFollower: function(toUser) {
		let fromUser = Meteor.userId();
		let followingList = UserCustom.findOne({userId:fromUser}).following;
		if ( followingList.indexOf(toUser) === -1 ){//if toUser not yet followed
			UserCustom.update({userId:fromUser},{$addToSet:{following:toUser}});//updates the stalker data
			UserCustom.update({userId:toUser},{$addToSet:{followers:fromUser}});//updates the celebrity
			let followersListOfClient = UserCustom.findOne({userId:fromUser}).followers;
			if( followersListOfClient.indexOf(toUser) !== -1 ){
				//if the celeb is already stalking the current stalker
				//updates the friendslist of both
				let nameAndUrl=UserCustom.findOne({userId:fromUser});//find name and url of the follower to be sent via notification
				UserCustom.update({userId:fromUser},{$addToSet:{friends:toUser}});
				UserCustom.update({userId:toUser},{$addToSet:{friends:fromUser}});
				Notifications.insert({	by:fromUser,	type:"following",	to:toUser, byName:nameAndUrl.name, byUrl:nameAndUrl.url });
			}
		}
		else{
			UserCustom.update({userId:fromUser},{$pull:{following:toUser}});
			UserCustom.update({userId:toUser},{$pull:{followers:fromUser}});

			let friendsList = UserCustom.findOne({userId:fromUser}).friends;
			if( friendsList.indexOf(toUser) !== -1 ){
				//if the celeb is already stalking the current stalker
				//updates the friendslist of both
				UserCustom.update({userId:fromUser},{$pull:{friends:toUser}});
				UserCustom.update({userId:toUser},{$pull:{friends:fromUser}});
			}
		}
	},
	userInfo:function (userUrl) {
		console.log(userUrl);
		return UserCustom.findOne({url:userUrl});
	},
	updateBio:function(text){
		let user=Meteor.userId();
		UserCustom.update({userId:user},{$set:{bio:text}},function(error,result){
			if (error) {
			} else {
			}
		});
	}
});

//Todo:
//Add error logging in this page