Meteor.publish("feedPosts", function(limit){
	if (this.userId) {
	  let followingList = UserCustom.findOne({userId:this.userId}).following;
		return Posts.find({owner: {$in:followingList}},{limit:limit});
	}
	else{
		return Posts.find({},{limit:limit});
	}
});
Meteor.publish("postsBy", function (author) {
    return Posts.find({owner:author});
  });
Meteor.publish("userPosts", function (url) {
    let userId = UserCustom.findOne({url:url}).userId;
    return Posts.find({owner:userId});
  });
Meteor.publish("userComments", function (forPost) {
    return UserComments.find({postId: forPost}, {sort: {createdAt: -1}});
});
Meteor.publish("userCustom", function () {
	return UserCustom.find({}, {fields: {name: 1, userId: 1, followers: 1, following: 1, friends: 1,url:1}});
});
Meteor.publish("currentUserDetails", function () {
	return UserCustom.find({userId: this.userId}, {fields: {userId: 1, name: 1, followers: 1, following: 1,url:1}});
});
Meteor.publish("userProfile", function (userUrl) {
	return UserCustom.find({url:userUrl}, {fields: {userId: 1, name: 1, followers: 1, following: 1,url:1,bio: 1,posts:1}});
});
Meteor.publish("friends", function () {
  	let friendsList = UserCustom.findOne({userId:this.userId}).friends;
  	return UserCustom.find({userId: {$in:friendsList}}, {fields: {name: 1, userId: 1}});
});
Meteor.publish("messagesWith", function (toUser) {
	let fromUser = this.userId;
    return Messages.find({
    	$or: [{toUser: toUser, fromUser: fromUser},{toUser: fromUser, fromUser: toUser}]},
    	{sort: {createdAt: -1}
    });
  });
Meteor.publish("notifications", function (forUser) {
    return Notifications.find({to:forUser});
  });
Meteor.publish("feeds", function(){
  return Feeds.find({}, {limit: 10});
})